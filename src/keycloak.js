import Keycloak from 'keycloak-js';

const keycloak = new Keycloak({
    url: 'http://localhost:8282', // URL of your Keycloak server
    realm: 'consultation_app_realm',          // Your realm name in Keycloak
    clientId: 'consultation_app_client',   // Your client ID in Keycloak
    credentials: {
        secret: 'XkehpwIXLNZY8DlIObTzHuLejiTJ0FqV' // Your client secret (replace with actual value)
    }
});

export default keycloak;
