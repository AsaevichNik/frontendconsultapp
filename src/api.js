import axios from 'axios';

const token = localStorage.getItem('token');

const api = axios.create({
    baseURL: 'http://localhost:8300/api',
    headers: {
        Authorization: `Bearer ${token}`
    }
});

export const registerUser = async (userData) => {
    try {
        // Отправляем POST запрос на ваш эндпоинт регистрации пользователя
        const response = await axios.post('http://localhost:8300/api/v1/auth/register', userData);
        // Если регистрация прошла успешно, возвращаем ответ
        return response.data;
    } catch (error) {
        // Если произошла ошибка при регистрации, выбрасываем исключение
        throw error;
    }
};

export const loginUser = async (userData) => {
    try {
        const response = await axios.post('http://localhost:8300/api/v1/auth/login', userData);
        return response.data;
    } catch (error) {
        throw error;
    }
};

export const getMentorsList = async () => {
    try {
        const response = await api.get('/mentor');
        return response.data;
    } catch (error) {
        throw error;
    }
};

export const fetchMentorDetails = async (mentorId, token) => {
    try {
        const response = await axios.get(`http://localhost:8300/api/v1/mentor/${mentorId}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    } catch (error) {
        throw error;
    }
};