import React, { useEffect, useState } from 'react';
import MentorCard from '../components/MainContent/MentorCard';

const PageSearch = () => {
    const [mentors, setMentors] = useState([]);

    useEffect(() => {
        fetchMentors();
    }, []);

    const fetchMentors = async () => {
        try {
            const token = localStorage.getItem('token');
            const response = await fetch('http://localhost:8300/api/v1/mentor', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });
            const data = await response.json();
            setMentors(data);
        } catch (error) {
            console.error('Error fetching mentors:', error);
        }
    };

    return (
        <div>
            {mentors.map((mentor) => (
                <MentorCard key={mentor.id} mentor={mentor} />
            ))}
        </div>
    );
};

export default PageSearch;
