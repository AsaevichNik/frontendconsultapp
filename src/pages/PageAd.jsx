import React, { useState, useEffect } from 'react';
import { useParams, useLocation } from 'react-router-dom';
import axios from 'axios';
import classes from './PageAd.module.scss';

const PageAd = () => {
    const { id } = useParams();
    const location = useLocation();
    const [mentor, setMentor] = useState(null);
    const [user, setUser] = useState(null);
    const [image, setImage] = useState(location.state?.image || null);

    useEffect(() => {
        fetchMentor();
        if (!image) {
            fetchImage();
        }
    }, [id]);

    const fetchImage = async () => {
        try {
            const token = localStorage.getItem('token');
            const response = await axios.get(`http://localhost:8300/api/v1/mentor/${id}/image`, {
                responseType: 'arraybuffer',
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            const base64 = btoa(
                new Uint8Array(response.data).reduce(
                    (data, byte) => data + String.fromCharCode(byte),
                    ''
                )
            );
            setImage(`data:image/jpeg;base64,${base64}`);
        } catch (error) {
            console.error('Error fetching image:', error);
        }
    };

    const fetchMentor = async () => {
        try {
            const token = localStorage.getItem('token');
            const response = await axios.get(`http://localhost:8300/api/v1/mentor/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            const data = response.data;
            console.log(data); // Логирование ответа API
            setMentor(data.mentor);
            setUser(data.user);
        } catch (error) {
            console.error('Error fetching mentor:', error);
        }
    };

    if (!mentor || !user) {
        return <div>Loading...</div>;
    }

    return (
        <div className={classes.mentor_details}>
            <h2>{user.fullName}</h2>
            <p>Email: {user.email}</p>
            <p>Birthday: {user.birthday}</p>
            {image && <img src={image} alt={user.fullName} className={classes.mentor_image} />}
            <h3>Experience</h3>
            <p>{mentor.experience}</p>
            <h3>How Can Help</h3>
            <p>{mentor.how_can_help}</p>
            <h3>Skills</h3>
            <p>{mentor.skills}</p>
        </div>
    );
};

export default PageAd;
