import React from 'react';
import Authorization from '../components/Autorization';

function AuthorizationPage() {
    return (
        <div>
            <Authorization />
        </div>
    );
}

export default AuthorizationPage;
