import React, { useState } from 'react';
import classes from './Search.module.scss';
import lupa from '../../assets/icons/lupa.svg';
import SearchResultsList from './SearchResultList';

export const SearchBar = () => {
    const [input, setInput] = useState('');
    const [results, setResults] = useState([]);

    const fetchData = async (value) => {
        try {
            const token = localStorage.getItem('token');
            const response = await fetch(`http://localhost:8300/api/v1/mentors?search=${value}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            });
            const data = await response.json();
            setResults(data);
        } catch (error) {
            console.error('Error fetching mentors:', error);
        }
    };

    const handleChange = (value) => {
        setInput(value);
        fetchData(value);
    };

    return (
        <div className={classes.search}>
            <div className={classes.search_icon}>
                <img src={lupa} alt="Search" />
            </div>
            <input
                className={classes.search_input}
                placeholder="Поиск"
                value={input}
                onChange={(e) => handleChange(e.target.value)}
            />
            <div className={classes.search_result}>
                {results && results.length > 0 && (
                    <SearchResultsList results={results} />
                )}
            </div>
        </div>
    );
};

export default SearchBar;
