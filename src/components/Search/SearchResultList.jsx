import React from 'react';
import classes from './SearchResultList.module.scss';

import { SearchResult } from "./SearchResult";

export const SearchResultsList = ({ results, setSelectedValue }) => {

  return (
      <div className={classes.resultss}>
        {results.map((result, id) => {
          return (
              <div
                  key={id}
                  className={classes['search-result']}
                  onClick={(e) => setSelectedValue(result.name)}
              >
                <div>{result.name}</div>
                <div>Email: {result.email}</div>
                <div>Опыт: {result.experience}</div>
                {/* Добавьте другие данные о результате поиска, если необходимо */}
              </div>
          );
        })}
      </div>
  );
};

export default SearchResultsList;
