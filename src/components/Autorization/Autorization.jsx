import React, { useState } from 'react';
import { registerUser } from '../../api';
import classes from './Autorization.module.scss';

function Authorization() {
    const [formData, setFormData] = useState({
        username: '',
        email: '',
        firstName: '',
        lastName: '',
        password: '',
        userRoleDto: '' // Исправлено на userRoleDto
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            await registerUser(formData);
        } catch (error) {
            console.error('Ошибка регистрации:', error);
        }
    };

    return (
        <div className={classes.authorization_main}>
            <div className={classes.authorization_headline}>Регистрация</div>
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    name="username"
                    placeholder="Логин"
                    className={classes.authorization_input}
                    value={formData.username}
                    onChange={handleChange}
                    required
                />
                <input
                    type="email"
                    name="email"
                    placeholder="Email"
                    className={classes.authorization_input}
                    value={formData.email}
                    onChange={handleChange}
                    required
                />
                <input
                    type="text"
                    name="firstName"
                    placeholder="Имя"
                    className={classes.authorization_input}
                    value={formData.firstName}
                    onChange={handleChange}
                    required
                />
                <input
                    type="text"
                    name="lastName"
                    placeholder="Фамилия"
                    className={classes.authorization_input}
                    value={formData.lastName}
                    onChange={handleChange}
                    required
                />
                <input
                    type="password"
                    name="password"
                    placeholder="Пароль"
                    className={classes.authorization_input}
                    value={formData.password}
                    onChange={handleChange}
                    required
                />
                <input
                    type="text"
                    name="userRoleDto"
                    placeholder="Роль"
                    className={classes.authorization_input}
                    value={formData.userRoleDto}
                    onChange={handleChange}
                    required
                />
                <button type="submit" className={classes.authorization_button}>
                    <div className={classes.authorization_button_text}>Продолжить</div>
                </button>
            </form>
        </div>
    );
}

export default Authorization;
