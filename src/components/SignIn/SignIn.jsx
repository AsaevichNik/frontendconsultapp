import React, { useState } from 'react';
import { loginUser } from '../../api';
import classes from './SignIn.module.scss';

function SignIn() {
    const [formData, setFormData] = useState({
        username: '',
        password: ''
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await loginUser(formData);
            localStorage.setItem('token', response.token);
            localStorage.setItem('refreshToken', response.refreshToken);
            localStorage.setItem('tokenType', response.tokenType);
            console.log('Вход выполнен успешно:', response);
        } catch (error) {
            console.error('Ошибка входа:', error);
        }
    };

    return (
        <div className={classes.signin_main}>
            <div className={classes.signin_headline}>Вход</div>
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    name="username"
                    placeholder="Логин"
                    className={classes.signin_input}
                    value={formData.username}
                    onChange={handleChange}
                    required
                />
                <input
                    type="password"
                    name="password"
                    placeholder="Пароль"
                    className={classes.signin_input}
                    value={formData.password}
                    onChange={handleChange}
                    required
                />
                <div className={classes.signin_link_main}>
                    <button type="button" className={classes.signin_link}>Забыли пароль?</button>
                    <button type="button" className={classes.signin_link}>У меня еще нет аккаунта</button>
                </div>
                <button type="submit" className={classes.signin_button}>
                    <div className={classes.signin_button_text}>Войти</div>
                </button>
            </form>
        </div>
    );
}

export default SignIn;
