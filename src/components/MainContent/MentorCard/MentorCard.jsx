import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import classes from './MentorCard.module.scss';

const MentorCard = ({ mentor }) => {
    const { id, fullName, experience, how_can_help, skills } = mentor;
    const [image, setImage] = useState(null);

    useEffect(() => {
        const fetchImage = async () => {
            try {
                const token = localStorage.getItem('token');
                const response = await axios.get(`http://localhost:8300/api/v1/mentor/${id}/image`, {
                    responseType: 'arraybuffer',
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                });
                const base64 = btoa(
                    new Uint8Array(response.data).reduce(
                        (data, byte) => data + String.fromCharCode(byte),
                        ''
                    )
                );
                setImage(`data:image/jpeg;base64,${base64}`);
            } catch (error) {
                console.error('Error fetching image:', error);
            }
        };
        fetchImage();
    }, [id]);

    return (
        <div className={classes.card}>
            <div className={classes.card_info}>
                <div className={classes.card_info_name}>{fullName}</div>
                <div className={classes.card_info_main}>
                    {image && <img src={image} alt={fullName} className={classes.card_image} />}
                    <div className={classes.description}>{experience}</div>
                    <div className={classes.description}>{how_can_help}</div>
                    <div className={classes.description}>{skills}</div>
                </div>
                <div className={classes.card_info_bottom}>
                    <Link to={`/PageAd/${id}`}>
                        <button className={classes.card_info_price_btn}>Подробнее</button>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default MentorCard;
