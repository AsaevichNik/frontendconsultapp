import React, { useEffect, useState } from 'react';
import classes from './MentorProfile.module.scss';
import axios from 'axios';

const MentorProfile = ({ mentor, user }) => {
    const { id, fullName, experience, how_can_help, skills } = mentor;
    const [image, setImage] = useState(null);

    useEffect(() => {
        const fetchImage = async () => {
            try {
                const token = localStorage.getItem('token');
                const response = await axios.get(`http://localhost:8300/api/v1/mentor/${id}/image`, {
                    responseType: 'arraybuffer',
                    headers: {
                        Authorization: `Bearer ${token}`
                    }
                });

                if (response.data) {
                    const base64 = Buffer.from(response.data, 'binary').toString('base64');
                    setImage(`data:image/jpeg;base64,${base64}`);
                } else {
                    console.error('Empty image data received.');
                }
            } catch (error) {
                console.error('Error fetching image:', error);
            }
        };
        fetchImage();
    }, [id]);

    return (
        <div className={classes.mentor_info}>
            <div className={classes.info_heading}>Карточка ментора</div>
            {user && (
                <div>
                    <p>Имя пользователя: {user.fullName}</p>
                    <p>Email: {user.email}</p>
                    <p>Дата рождения: {user.birthday}</p>
                </div>
            )}
            {mentor && (
                <div>
                    {image && <img src={image} alt={fullName} className={classes.mentor_image} />}
                    <div className={classes.form_row}>
                        <label>Опыт работы:</label>
                        <textarea
                            className={classes.input}
                            value={experience}
                            readOnly
                        />
                    </div>
                    <div className={classes.form_row}>
                        <label>С чем могу помочь:</label>
                        <textarea
                            className={classes.input}
                            value={how_can_help}
                            readOnly
                        />
                    </div>
                    <div className={classes.form_row}>
                        <label>Навыки:</label>
                        <input
                            type="text"
                            className={classes.input}
                            value={skills}
                            readOnly
                        />
                    </div>
                </div>
            )}
        </div>
    );
};

export default MentorProfile;
