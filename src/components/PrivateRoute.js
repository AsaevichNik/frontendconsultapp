import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';
import { useKeycloak } from '@react-keycloak/web';

const PrivateRoute = ({ component: Component, ...rest }) => {
    const { keycloak } = useKeycloak();
    const location = useLocation();

    return (
        keycloak.authenticated ? <Component {...rest} /> : <Navigate to="/AutorizationPage" state={{ from: location }} />
    );
};

export default PrivateRoute;
